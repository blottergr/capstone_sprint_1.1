from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('emails.views',
    url(r'^sendTestEmail$', 'sendTestEmail'),
    (r'^confirm','confirm_email'),
)