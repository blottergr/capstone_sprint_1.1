from django.db import models
import datetime
from django.core.mail import send_mail

# Create your models here.


class EmailTemplate(models.Model):
	TYPE_CHOICES = ( #the ids will be used to know what mixins/piped text will be used
		 ('1', 'Notify Preceptor of Placement'),
		 ('2', 'Notify Student of Placement'),
		 ('3', 'Thank Preceptor for Work'),
	)
	name = models.CharField(max_length=140) #defaults to type in interface, but can be changed by user
	type = models.CharField(max_length=1, choices=TYPE_CHOICES)

	hot_tags_for_email_template = 'Tags: &LT;student_first_name&GT;, &LT;student_last_name&GT;, &LT;student_class&GT;, &LT;preceptor_name&GT;, &LT;current_semester&GT;'
	body = models.TextField(help_text = hot_tags_for_email_template)

	def __unicode__(self):
		return self.name

class EmailHistory(models.Model):
	template = models.ForeignKey(EmailTemplate)
	placement = models.ForeignKey('school.Placement')
	email_sent = models.DateTimeField(blank=True, null=True)
	email_message = models.TextField(help_text='Body of the message sent to the recipient')
# 	RECIPIENT_CHOICES = ( #select if the email should be sent to the student or preceptor
# 		('S', 'Student'),
# 		('P', 'Preceptor'),
# 	)
#	recipient = models.CharField(max_length=1, choices=RECIPIENT_CHOICES)
	recipient_email = models.CharField(max_length=140, help_text='This will be auto populated with the Student\'s or Preceptor\'s email')
	#cc = models.CharField(max_length=140) # optional field for copying someone else
	def populate_fields(self):
		if self.email_sent == None: # If this is True than the email hasn't been sent yet.
			# Get the type
			if self.template.type == '1':
				# Get the preceptor's email
				self.recipient_email = self.placement.preceptor.email
			else:
				# Get the student's email
				self.recipient_email = self.placement.student.email
			# Parse the message to replace tags with values
			self.email_message = self.parse_message()
			
			# Show Confirmation page??
			
			# If confirm, send email and save history
			#self.email_sent = datetime.datetime.now()
			self.save()
			
			# Else just delete this entry
			
	
	def parse_message(self):
		message = self.template.body
		message = message.replace('<student_first_name>', self.placement.student.first_name)
		message = message.replace('<student_last_name>', self.placement.student.last_name)
		
		preceptor_name = self.placement.preceptor.first_name + ' ' + self.placement.preceptor.last_name
		message = message.replace('<preceptor_name>', preceptor_name)
		message = message.replace('<current_semester>', self.placement.term.term_name)
		
		return message
	
	def send_email(self):
		# Send the email
		send_mail('Placement Notification', self.email_message, 'nursingcapstone@gmail.com', [self.recipient_email], fail_silently=False)
		self.email_sent = datetime.datetime.now()
		self.save()
		#print 'send the email'
		
	
