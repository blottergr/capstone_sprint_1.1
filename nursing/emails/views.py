from django.http import HttpResponse, HttpResponseRedirect
from emails.forms import *
from django.contrib import messages

def sendTestEmail(request):
	from django.core.mail import send_mail, mail_admins
	# from settings import ADMINS
	# send_mail('Subject here', 'Here is the message.', 'from@example.com', [ADMINS], fail_silently=False)

	mail_admins('Test email for the admins', 'This is a test email.', fail_silently=False)

	return HttpResponse("An email was sent to the site admins")

def confirm_email(request):
	path = request.GET['redirect']
	emails = EmailFormSet(request.POST)
	if emails.is_valid():
		count = 0
		for email in emails:
			EmailObject = email.save()
			EmailObject.send_email()
			count += 1
				
			plural = ''
			if count != 1:
				plural = 's'
		messages.success(request, "Successfully sent %s email%s." % (count, plural))
		return HttpResponseRedirect(path)
		#return HttpResponseRedirect(request.META['HTTP_REFERER'])
	else:
		return render_to_response('admin/confirm_emails.html', {'emailforms': emails }, context_instance=RequestContext(request))