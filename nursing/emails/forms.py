from django import forms
from django.forms.models import modelformset_factory
from emails.models import *

class EmailHistoryForm(forms.ModelForm):
	class Meta:
		model=EmailHistory
		exclude = ('template', 'placement', 'email_sent',)
		fields = ('recipient_email', 'email_message',)
	
EmailFormSet = modelformset_factory(EmailHistory, form=EmailHistoryForm, extra=0)