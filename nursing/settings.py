# Django settings for nursing project.
# import os
# ROOT_PATH = os.path.dirname(__file__)
from os.path import abspath
TEMPLATE_DEBUG = DEBUG = False

MANAGERS = ADMINS = (
    ('Wes Burningham', 'wburningham+capstone@gmail.com'),
    ('Kenny Fernsten', 'kfernsten@gmail.com'),
    ('Nate Salisbury', 'nathanpsalisbury@gmail.com'),
    ('Geoff Blotter', 'blotterg@gmail.com'),
)


EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_PASSWORD = 'nursesrock'
EMAIL_HOST_USER = 'nursingcapstone@gmail.com'
EMAIL_PORT = 587
EMAIL_SUBJECT_PREFIX = 'SwanPaw: '
EMAIL_USE_TLS = True


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', 
        'NAME': 'nursing',                    
        'USER': 'django', 
        'PASSWORD': '9bK8KzbDhh72zKmJ', 
        'HOST': '127.0.0.1',                     
        'PORT': '3306',                     
    }
}

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# On Unix systems, a value of None will cause Django to use the same
# timezone as the operating system.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/Denver'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SESSION_EXPIRE_AT_BROWSER_CLOSE = True
SESSION_COOKIE_AGE = 86400

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = False

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = False

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = abspath('media/')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = '/media/'

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"
STATIC_ROOT = '/var/www/media/'

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'

# URL prefix for admin static files -- CSS, JavaScript and images.
# Make sure to use a trailing slash.
# Examples: "http://foo.com/static/admin/", "/static/admin/".
# ADMIN_MEDIA_PREFIX = '/static/admin/'
ADMIN_MEDIA_PREFIX = STATIC_URL + "grappelli/"

# Additional locations of static files
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    abspath('static/'),

)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
#    'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'g_yxr-)ih8u0om-+zy+27pcpzb113%i1pup35)=7bk)h*gxu48'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
#     'django.template.loaders.eggs.Loader',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
)

ROOT_URLCONF = 'nursing.urls'

TEMPLATE_DIRS = (
    abspath('templates/'),
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'grappelli', #Must be before django.contrib.admin
    'django.contrib.admin',
    'school',
    'south',
    'emails',
    'people',
    'byu',
    # Uncomment the next line to enable admin documentation:
    # 'django.contrib.admindocs',
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'CAS': {
            'handlers': ['console'],
            'level': 'DEBUG',
        }
    }
}

#Grappelli settings
GRAPPELLI_ADMIN_HEADLINE = GRAPPELLI_ADMIN_TITLE = "SwanPaw"




#http://goo.gl/ZeJpF
#also see readme from download http://goo.gl/VUlhe
USE_CAS = True

## django_cas settings
if USE_CAS:
	CAS_SERVER_URL = 'https://cas.byu.edu/cas/'
	CAS_VERSION = '2'
	CAS_REDIRECT_URL = 'http://it.et.byu.edu:44685'
	#CAS_REDIRECT_URL = 'https://it.et.byu.edu:446485'
	CAS_IGNORE_REFERER = True

	AUTHENTICATION_BACKENDS = (
		'django.contrib.auth.backends.ModelBackend',
		'django_cas.backends.CASBackend',
	)

	MIDDLEWARE_CLASSES += (
		'django_cas.middleware.CASMiddleware',
		'django.middleware.doc.XViewMiddleware',
	)


try:
	from local_settings import *
except ImportError:
	pass	
