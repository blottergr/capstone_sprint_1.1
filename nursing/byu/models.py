class Request:

    def __init__(self):
        try:
            from settings import BYU_WEB_SERIVCE_KEY, BYU_WEB_SERIVCE_ID
            self.key = BYU_WEB_SERVICE_KEY
            self.web_service_id = BYU_WEB_SERVICE_ID
        except ImportError:
            self.key = 'uOahoLdrYwf94srS-e_rUUdJ1t6ho0Zi_0uozdVB'
            self.web_service_id  = 'Cz2dMqlhqqbJ5Fy_BhK7'
            #self.web_service_id = 'N24oC4iAoV_CPB41AKKU'
            #self.key = 'gK4TAFfkH0-gbWHnmgec4nVzzvj6Geft6jcDn2Az'

    def Get(self, url=None, data=None, actor=''):
        """
        This function makes a GET request to a BYU web service using
        HMAC URL ENCODED authentication

        Examples:

        >>> url = 'example'
        >>> data = ''
        >>> actor = ''
        >>> x = Request()
        >>> x.Get(url, data)
        '{"greeting":"Hello There Jarrett Burningham"}'

        """
        #Check for required argments
        if url is None:
            raise Exception('Missing url argument')

        import urllib2


        #Prfix with comma for authorization header
        actor =  ',' + actor if actor != '' else ''  

        #Check to see if the quick name for an endpoint is used; if so, use it
        url = self.GetEndPoint(url)

        url = str(url) + data

        #Create timestamp and hash
        timestamp = self._Now()
        msg = str(url) + str(timestamp)
        #my_hash = base64.b64encode(hmac.new(self.key, msg, hashlib.sha512).digest())
        my_hash = self._CreateHash(msg)

        #Create authorizatoin header
        authorization_header = "URL-Encoded-API-Key %s,%s,%s%s" % (self.web_service_id, my_hash, timestamp, actor)

        #Create the request object and set some headers
        req = urllib2.Request(url)
        req.add_header('Accept', 'application/json')
        req.add_header("Content-type", "application/x-www-form-urlencoded")
        req.add_header('Authorization', authorization_header)

        #Make the request and return the results
        res = urllib2.urlopen(req)
        return res.read()


    def Post(self, url=None, data=None, actor=''):
        """
        This function makes a POST request to a BYU web service using
        HMAC NONCE ENCODED authentication

        Examples:

        >>> url = 'example'
        >>> data = ''
        >>> actor = ''
        >>> x = Request()
        >>> x.Post(url, data)
        '{"greeting":"knock knock, who is there? Jarrett Burningham who?"}'

        """
        #Check for required argments
        if url is None:
            raise Exception('Missing url argument')

        import urllib2
        from urllib import urlencode

        if actor != '':
            actor = '/'+actor

        #Build nonce request
        nonce_url = 'https://ws.byu.edu/authentication/services/rest/v1/hmac/nonce/%s%s' % (self.web_service_id, actor)
        nonce_req = urllib2.Request(nonce_url)
        nonce_req.add_header('Accept', 'application/json')
        nonce_req.add_header("Content-type", "application/x-www-form-urlencoded")
        #nonce_req.add_header("Content-type", "application/x-www-form-urlencoded; charset=UTF-8")

        #Make nonce request and return the results
        nonce_req = urllib2.urlopen(nonce_req, urlencode(''))
        nonce_string = nonce_req.read()
        
        #Prefix with comma for authorization header
        actor =  ',' + actor if actor != '' else ''  

        import json
        nonce = json.loads(nonce_string)

        nonce_key = nonce['nonceKey']
        nonce_value = nonce['nonceValue']

        #Check to see if the quick name for an endpoint is used; if so, use it
        url = self.GetEndPoint(url)

        #Create hash from nonce_value
        my_hash = self._CreateHash(nonce_value)

        #Create authorizatoin header
        authorization_header = "Nonce-Encoded-API-Key%s,%s,%s" % (self.web_service_id, nonce_key, my_hash) 

        #Create the request object and set some headers
        req = urllib2.Request(url)
        req.add_header('Accept', 'application/json')
        req.add_header("Content-type", "application/x-www-form-urlencoded")
        req.add_header('Authorization', authorization_header)

        #Make the request and return the results
        res = urllib2.urlopen(req, data)

        return res.read()


    def _CreateHash(self, msg=None):
        """
        Creates a hash using a base_64 encoding of a SHA256 hash on the 
        msg and the BYU_WEB_SERVICE_KEY

        Examples:

        #>>> msg = 'cat'
        #>>> x = Request()
        #>>> x._CreateHash(msg)
        #'V4wzu7WJPFThyfp2ulhVoItSK/MeOg4fWP1STdzrsxRpyi16GOw0AAZJwMMa/a6zbwOsRZjDHz6nvzW7/Jciyg=='

        """
        if msg is None:
            raise Exception('Missing message for hash')

        import hashlib, hmac, base64

        return base64.b64encode(hmac.new(self.key, msg, hashlib.sha512).digest())

    def _Now(self):
        from time import strftime, localtime
        #Return timestmap using BYU OIT's format
        return strftime("%Y-%m-%d %H:%M:%S",localtime())

    def _CleanUrl(self, url=None):
        """
        Replaces spaces with correct characters

        Examples:

        #>>> url = None
        #>>> x = Request()
        #>>> x.GetEndPoint(url)
        #'Traceback (most recent call last):'
        >>> url = 'net_id'
        >>> x = Request()
        >>> x.GetEndPoint(url)
        'https://ws.byu.edu/rest/v1/identity/person/netIdService/'

        """
        if url is None:
            raise Exception('A url must be provided')
        
        return url


    def GetEndPoint(self, url=None):
        """
        Looks up endpoints in mapping. If not found it will return the url

        Examples:

        #>>> url = None
        #>>> x = Request()
        #>>> x.GetEndPoint(url)
        #'Traceback (most recent call last):'
        >>> url = 'net_id'
        >>> x = Request()
        >>> x.GetEndPoint(url)
        'https://ws.byu.edu/rest/v1/identity/person/netIdService/'

        """
        if url is None:
            raise Exception('A url must be provided')
    
        endpoints = {
            'example':'https://ws.byu.edu/example/authentication/hmac/services/v1/exampleWS',
            'summary':'https://ws.byu.edu/rest/v1/identity/summary/',
            'reg_offerings':'https://ws.byu.edu/rest/v1/academic/registration/regOfferings/',
            'address':'https://ws.byu.edu/rest/v1/identity/person/address/',
            'change_password':'https://ws.byu.edu/rest/v1/identity/person/changePassword/',
            'code_maintenance':'https://ws.byu.edu/rest/v1/identity/person/codeMaintenance/',
            'net_id':'https://ws.byu.edu/rest/v1/identity/person/netIdService/',
            'language':'https://ws.byu.edu/rest/v1/identity/person/language/',
            'gro_is_member':'https://ws.byu.edu/rest/v1/identity/person/isMember/',
            'gro_members_of':'https://ws.byu.edu/rest/v1/identity/person/membersOf/',
            'person_id_card':'https://ws.byu.edu/rest/v1/identity/person/personIdCard/',
            'person_bio':'https://ws.byu.edu/rest/v1/identity/person/personBio/',
            'person_ids':'https://ws.byu.edu/rest/v1/identity/person/personIds/',
            'person_lookup':'https://ws.byu.edu/rest/v1/identity/person/personLookup/',
            'person_names':'https://ws.byu.edu/rest/v1/identity/person/personNames/',
            'person_photo':'https://ws.byu.edu/rest/v1/identity/person/personPhoto/',
            'person_summary':'https://ws.byu.edu/rest/v1/identity/person/personSummary/',
            'person_relationship':'https://ws.byu.edu/rest/v1/identity/person/relationship/',
            'person_unlisted_info':'https://ws.byu.edu/rest/v1/identity/person/unlistedInfo/'
        }   
        
        return endpoints[url] if url in endpoints else url  

#Run doctests if runs as a standalone script
if __name__ == '__main__':
     import doctest
     doctest.testmod()

