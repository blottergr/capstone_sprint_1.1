from django.conf.urls.defaults import patterns, include, url
from django.views.generic.simple import redirect_to
import os

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # ('^admin/school/placement/[^/?]', redirect_to, {'url': '/admin/school/placement/?term__exact=20125', 'permanent': False}),

    url(r'^error$', 'django_cas.views.login', name='hofdfdme'),
    (r'^$', redirect_to, {'url': 'admin/', 'permanent': False}),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    
    url(r'^emails/', include('emails.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    (r'^grappelli/', include('grappelli.urls')),
    (r'^eval/', include('evaluations.urls')),
)



import settings
# django_cas
if settings.USE_CAS:
	urlpatterns = patterns('',
		('^admin/logout/$', 'django.views.generic.simple.redirect_to',
			{'url': '../../accounts/logout'}),
		('^admin/password_change/$', 'django.views.generic.simple.redirect_to',
			{'url': 'https://it.byu.edu/byu/help.do?&sysparm_document_key=kb_knowledge,d414cfba0a0a3c0e5e863c773262f4a9'}),
				url(r'^accounts/login/$', 'django_cas.views.login',name='login'),
				url(r'^accounts/logout/$', 'django_cas.views.logout',name='logout'),
		) + urlpatterns

# end django_cas

# to serve media
if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^%s(?P<path>.*)$' % settings.MEDIA_URL[1:], 'django.views.static.serve', {'document_root': os.path.abspath('media')}),
    )

