from school.models import *
from emails.models import *
from emails.forms import *
from django.contrib import admin
from django.shortcuts import render_to_response
from django.template import RequestContext
from django import forms
from django.forms.models import modelformset_factory

from django.db import transaction
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect


from logging import getLogger

# Get an instance of a logger
logger = getLogger('CAS')

def get_term(object):
    current_term = object.term[4:]
    if current_term == '1':
        return 'Fall'
    elif current_term == '3':
        return 'Spring'
    elif current_term == '4':
        return 'Summer'
    elif current_term == '5':
        return 'Winter'

get_term.short_description="Term"

def get_year(object):
    return object.term[:4]

get_year.short_description="Year"

class EmailHistoryInline(admin.TabularInline):
	model = EmailHistory
	extra = 0
	readonly_fields = ('recipient_email', 'email_message', 'email_sent',)


class PlacementAdmin(admin.ModelAdmin):
	list_filter = ['term']
	search_fields = ['student', 'site', 'preceptor', 'term']
	list_display = ['student', 'site', 'preceptor', 'term']
	list_display_links = ['student', 'site', 'preceptor', 'term']
	

	raw_id_fields = ('student','site','preceptor')
	# define the autocomplete_lookup_fields
	autocomplete_lookup_fields = {
		'fk': ['student', 'preceptor','site'],

	}
	inlines = [
		EmailHistoryInline,
	]

	def confirm_send_emails(self, request, queryset1, redirect_object):
		from emails.forms import * 
		formset = EmailFormSet(queryset=queryset1.emailhistory_set.filter(email_sent=None))
		return render_to_response('admin/confirm_emails.html', {'emailforms': formset, 'path': redirect_object }, context_instance=RequestContext(request))
	
	def response_change(self, request, queryset):
		# Check if there are emails that haven't been sent
		if queryset.emailhistory_set.filter(email_sent=None):
			# Generate the email details
			queryset.populate_unsent_email_details()
			# Get the return path
			result = super(PlacementAdmin, self).response_change(request, queryset)
			import os
			path = os.path.join(request.path, result['location'])
			# Go to the confirm page
			return self.confirm_send_emails(request, queryset, path)
		else:
			result = super(PlacementAdmin, self).response_change(request, queryset)
			return result
	def response_add(self, request, queryset):
		# Check if there are emails that haven't been sent
		if queryset.emailhistory_set.filter(email_sent=None):
			# Generate the email details
			queryset.populate_unsent_email_details()
			# Get the return path
			result = super(PlacementAdmin, self).response_add(request, queryset)
			import os
			path = os.path.join(request.path, result['location'])
			# Go to the confirm page
			return self.confirm_send_emails(request, queryset, path)
		else:
			result = super(PlacementAdmin, self).response_add(request, queryset)
			return result



class SiteContractAdminInline(admin.TabularInline):
    model = SiteContract
    extra = 1


class SiteAdmin(admin.ModelAdmin):
    inlines = (SiteContractAdminInline,)




admin.site.register(Site, SiteAdmin)

admin.site.register(Placement, PlacementAdmin)
#admin.site.register(SiteContract)

