from django.db import models
from django.contrib.auth.models import User
from django.utils.encoding import smart_str
import os

#placement, site




class Site(models.Model):
	name = models.CharField(max_length=140)
	#address
	street = models.CharField(max_length=200,blank=True,null=True)
	city = models.CharField(max_length=100,blank=True,null=True)
	zip_code = models.CharField(max_length=10,blank=True,null=True)
	phone = models.CharField(max_length=25,blank=True,null=True)
	fax = models.CharField(max_length=25,blank=True,null=True)
	email = models.EmailField(blank=True,null=True)
	office_manager = models.CharField(max_length=14,blank=True,null=True)
	office_manager_phone = models.CharField(max_length=25,blank=True,null=True)
	office_manager_email = models.CharField(max_length=25,blank=True,null=True)
	#questions = models.ManyToManyField('Attributes', Question)

	def hasContract():
		return True #This should check to see if it has a current contract

	def __unicode__(self):
		return self.name

	def related_label(self):
		return self.name

	def __str__(self):
		return self.name
		
	@staticmethod
	def autocomplete_search_fields():
		return ("id__iexact", "name__icontains",)

class SiteContract(models.Model):
	site = models.ForeignKey(Site)
	expiration_date = models.DateField()
	contract_file = models.FileField(upload_to='contracts');


def get_term():

    return (('1','test'),('2','test'),)


class Placement(models.Model):
    student = models.ForeignKey('people.Student')	
    preceptor = models.ForeignKey('people.Preceptor')	
    #site could be accessed through preceptor, but that relationship could change
    site = models.ForeignKey(Site)	#This should not be displayed to user when creating placement, this should be auto set
    #What is the format of a term?

    
    term = models.ForeignKey('Term') #this should be defualted to current term, but user has option to change


    def __unicode__(self):
		# return self.student.__str__() + " with " + self.preceptor.__str__() + " at " + self.site.__str__() + " during " + self.term
        return '%s with %s at %s during %s' % (self.student, self.preceptor, self.site, self.term)
	
    def populate_unsent_email_details(self):
        # Check each email history to see if any emails need to be sent
        for email in self.emailhistory_set.all():
            email.populate_fields()





class Term(models.Model):
    term_code = models.IntegerField()
    term_name = models.CharField(max_length=20)
    is_current = models.BooleanField(default=False)

    def __unicode__(self):
        return self.term_name



"""
class Course(models.Model):
	name = models.CharField(max_length=140)
	#questions = models.ManyToManyField(Question)

	def __unicode__(self):
		return self.name
"""


# Django foreign key default value example http://goo.gl/V6U7q
# class Foo(models.Model):
#     a = models.CharField(max_length=10)

# def get_foo():
#     return Foo.objects.get(id=1)

# class Bar(models.Model):
#     b = models.CharField(max_length=10)
#     a = models.ForeignKey(Foo, default=get_foo)



#TODO
#site demographics
#site hasContract method
#siteContract file upload stuff
#set defualt term for when creating a placement
#decide the format for term 20115 vs Winter 2011
#does a site have many contracts or should a contract be an attribute of the site?
