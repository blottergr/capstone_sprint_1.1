from people.models import *
from django.contrib import admin




class FacultyAdmin(admin.ModelAdmin):
	exclude = ('password', 'is_staff', 'is_superuser', 'last_login', 'date_joined', 'groups', 'user_permissions')
	list_display = ['first_name', 'last_name', 'username']
	search_fields = ['first_name', 'last_name', 'username']

class StudentAdmin(admin.ModelAdmin):
	exclude = ('password', 'is_staff', 'is_superuser', 'last_login', 'date_joined', 'groups', 'user_permissions', 'person_num')
	list_display = ['first_name', 'last_name', 'username']
	search_fields = ['first_name', 'last_name', 'username']
	 
class PreceptorAdmin(admin.ModelAdmin):
	exclude = ('is_staff', 'is_superuser', 'last_login', 'date_joined', 'groups', 'user_permissions')
	list_display = ['first_name', 'last_name', 'site']
    

class PersonAdmin(admin.ModelAdmin):
	exclude = ('is_staff', 'is_superuser', 'last_login', 'date_joined', 'groups', 'user_permissions')
	list_display = ['last_name', 'first_name', 'net_id', 'roles']


admin.site.register(Faculty, FacultyAdmin)
admin.site.register(Student, StudentAdmin)
admin.site.register(Preceptor, PreceptorAdmin)

admin.site.register(Person, PersonAdmin)
