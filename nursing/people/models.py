from django.db import models
from django.contrib.auth.models import User, Group
from django.utils.encoding import smart_str
from django.core.exceptions import ObjectDoesNotExist



# Create your models here.


class Person(User):
    class Meta:
        ordering = ["last_name"]
        proxy = True
        verbose_name_plural = "People"

    def __unicode__(self):
        return self.last_name + ", " + self.first_name
    
    def roles(self):
        return "User"
    
    def net_id(self):
        return self.username
    net_id.short_description = 'Net ID'

    def name(self):
        return self.first + " " + self.last

    

class Student(User):
    byu_num = models.CharField(max_length=9, blank=True)
    person_num = models.CharField(max_length=11,blank=True)
    hair = models.CharField(max_length=11,blank=True)
    hair_color = models.CharField(max_length=11)


    def __unicode__(self):
        return self.last_name + ", " + self.first_name
    
    def related_label(self):
        return self.last_name + ", " + self.first_name
    
    def __str__(self):
        return smart_str('%s %s' % (self.first_name, self.last_name))
    
    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "first_name__icontains",'last_name__icontains')   

    def save(self, *args, **kwargs):
        #Add student to student group for permissions
        super(Student, self).save(*args, **kwargs) # Call the "real" save() method.
        try:
            g = Group.objects.get(name='student') 
            g.user_set.add(self)
        except ObjectDoesNotExist:
            pass #TODO log error
        

    class Meta:
        permissions = (("is_a_student", "Is a Student"),)

class Faculty(User):
    byu_num = models.CharField(max_length=9, blank=True)

    class Meta:
        verbose_name_plural = "Faculty"

    def __unicode__(self):
        return self.last_name + ", " + self.first_name
    
    def related_label(self):
        return self.last_name + ", " + self.first_name
    
    def __str__(self):
        return smart_str('%s %s' % (self.first_name, self.last_name))
    
    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "first_name__icontains",'last_name__icontains')

    def save(self, *args, **kwargs):
        #Add faculty to faculty group for permissions
        super(Faculty, self).save(*args, **kwargs) # Call the "real" save() method.
        try:
            g = Group.objects.get(name='faculty') 
            g.user_set.add(self)
        except ObjectDoesNotExist:
            pass #TODO log error

    class Meta:
        permissions = (("is_a_faculty", "Is a Faculty"),)

class Preceptor(User):
    site = models.ForeignKey('school.Site') 

    def __unicode__(self):
        return self.last_name + ", " + self.first_name
    
    def related_label(self):
        return self.last_name + ", " + self.first_name
    
    def __str__(self):
        return smart_str('%s %s' % (self.first_name, self.last_name))
    
    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "first_name__icontains",'last_name__icontains')

    def save(self, *args, **kwargs):
        #Add preceptor to preceptor group for permissions
        super(Preceptor, self).save(*args, **kwargs) # Call the "real" save() method.
        try:
            g = Group.objects.get(name='preceptor') 
            g.user_set.add(self)
        except ObjectDoesNotExist:
            pass #TODO log error

    class Meta:
        permissions = (("is_a_preceptor", "Is a Preceptor"),)
