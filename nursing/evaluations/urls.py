from django.conf.urls.defaults import *

urlpatterns = patterns("evaluations.views",
    url(r"(?P<type>.*)/complete/$", "complete", name="complete"),
    url(r"(?P<type>.*)/$", "view", name="view"),



    (r'^$', 'index'),
)
