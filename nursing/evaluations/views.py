
from django.conf import settings
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.core.mail import EmailMessage
from django.core.urlresolvers import reverse
from django.shortcuts import get_object_or_404, redirect, render_to_response
from django.template import RequestContext
from django.utils.http import urlquote

#from evaluations.forms import FormForForm
from evaluations.models import Template
#from evaluations.signals import form_invalid, form_valid



def index(request):
    return render_to_response ('front/index.html',{},context_instance=RequestContext(request))


def view(request, type, template="evaluations/view.html"):
    """
    Display a built evaluation and handle submission.
    """
    form = get_object_or_404(Template, type=type)

    # if form.login_required and not request.user.is_authenticated():
    #     return redirect("%s?%s=%s" % (settings.LOGIN_URL, REDIRECT_FIELD_NAME,
    #                     urlquote(request.get_full_path())))
    args = (form, request.POST or None, request.FILES or None)
    form_for_form = FormForForm(*args)
    if request.method == "POST":
        if not form_for_form.is_valid():
            # form_invalid.send(sender=request, form=form_for_form)
            pass
        else:
            entry = form_for_form.save()
            fields = ["%s: %s" % (v.label, form_for_form.cleaned_data[k])
                for (k, v) in form_for_form.fields.items()]
            # subject = form.email_subject
            # if not subject:
            #     subject = "%s - %s" % (form.title, entry.entry_time)
            # body = "\n".join(fields)
            # if form.email_message:
            #     body = "%s\n\n%s" % (form.email_message, body)
            # email_from = form.email_from or settings.DEFAULT_FROM_EMAIL
            # email_to = form_for_form.email_to()
            # if email_to and form.send_email:
            #     msg = EmailMessage(subject, body, email_from, [email_to])
            #     msg.send()
            # email_from = email_to or email_from # Send from the email entered.
            # email_copies = [e.strip() for e in form.email_copies.split(",")
            #     if e.strip()]
            # if email_copies:
            #     msg = EmailMessage(subject, body, email_from, email_copies)
            #     for f in form_for_form.files.values():
            #         f.seek(0)
            #         msg.attach(f.name, f.read())
            #     msg.send()
            # form_valid.send(sender=request, form=form_for_form, entry=entry)
            return redirect(reverse("form_sent", kwargs={"type": form.type}))
    context = {"form": form, "form_for_form": form_for_form}
    return render_to_response(template, context, RequestContext(request))


def complete(request, type, template="evaluations/complete.html"):
    """
    Show the response message.
    """
    form = get_object_or_404(Template, type=type)

    context = {"form": form}
    return render_to_response(template, context, RequestContext(request))
