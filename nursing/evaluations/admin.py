
from csv import writer
from datetime import datetime
from mimetypes import guess_type
from os.path import join
from cStringIO import StringIO

from django.conf.urls.defaults import patterns, url
from django.contrib import admin
from django.core.files.storage import FileSystemStorage
from django.core.urlresolvers import reverse
from django.db.models import Count
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.template.defaultfilters import slugify
from django.utils.translation import ungettext, ugettext_lazy as _
#from django.forms import ModelForm, HiddenInput

# from evaluations.forms import EntriesForm
from evaluations.models import *
from evaluations.settings import EVALUATIONS_CSV_DELIMITER, EVALUATIONS_UPLOAD_ROOT

fs = FileSystemStorage(location=EVALUATIONS_UPLOAD_ROOT)
form_admin_filter_horizontal = ()

form_admin_fieldsets = [
    (None, 
    {"fields": ("title", 
    # "login_required", 
    "type",
        ("start_date", "end_date",), )}),]

from django.forms import ModelForm, HiddenInput


class QuestionForm(ModelForm):

     class Meta:
         model = Question
         fields = ("label", "field_type", "required", "options", "position",)

         widgets = {
             'position': HiddenInput(),
         }


class QuestionInline(admin.TabularInline):
    #fields = ("field_type", "label", "required", "options", "position",)
    model = Question
    form = QuestionForm
    extra = 1
    sortable_field_name = "position"
    raw_id_fields = ('options',)
    # define the autocomplete_lookup_fields
    autocomplete_lookup_fields = {
        'm2m': ['options'],
    }
    #widgets = {
    #    'position': HiddenInput(),
    #}  



class SectionForm(ModelForm):
    class Meta:
        model = Section
        fields = ("label",'template', 'position')
        widgets = {
            'position': HiddenInput(),
        }



class SectionInline(admin.TabularInline):

    model = Section
    #fields = ('label','template','position')
    form = SectionForm
    extra = 0
    sortable_field_name = "position"
    #raw_id_fields = ('label',)
    # define the autocomplete_lookup_fields
    #autocomplete_lookup_fields = {
        
    #   'fk': ['label'],
    #}


class SectionAdmin(admin.ModelAdmin):
    inlines = (QuestionInline,)

class TemplateAdmin(admin.ModelAdmin):

    inlines = (SectionInline,)
    list_display = ("title", "start_date", "end_date", "active",)
    list_display_links = ("title",)
    # list_editable = ("start_date", "end_date")
    list_filter = ("type",)
    filter_horizontal = form_admin_filter_horizontal
    search_fields = ("title",)
    # radio_fields = {"status": admin.HORIZONTAL}
    fieldsets = form_admin_fieldsets

    # def queryset(self, request):
    #     """
    #     Annotate the queryset with the entries count for use in the
    #     admin list view.
    #     """
    #     qs = super(FormAdmin, self).queryset(request)
    #     return qs.annotate(total_responses=Count("responses"))

    # def get_urls(self):
    #     """
    #     Add the entries view to urls.
    #     """
    #     urls = super(FormAdmin, self).get_urls()
    #     extra_urls = patterns("",
    #         url("^(?P<form_id>\d+)/entries/$",
    #             self.admin_site.admin_view(self.entries_view),
    #             name="form_responses"),
    #         url("^file/(?P<field_entry_id>\d+)/$",
    #             self.admin_site.admin_view(self.file_view),
    #             name="form_file"),
    #     )
    #     return extra_urls + urls

    # def entries_view(self, request, form_id):
    #     """
    #     Displays the form entries in a HTML table with option to
    #     export as CSV file.
    #     """
    #     if request.POST.get("back"):
    #         change_url = reverse("admin:%s_%s_change" %
    #             (Template._meta.app_label, Template.__name__.lower()), args=(form_id,))
    #         return HttpResponseRedirect(change_url)
    #     form = get_object_or_404(Template, id=form_id)
    #     entries_form = EntriesForm(form, request, request.POST or None)
    #     delete_entries_perm = "%s.delete_formentry" % Response._meta.app_label
    #     can_delete_entries = request.user.has_perm(delete_entries_perm)
    #     submitted = entries_form.is_valid()
    #     if submitted:
    #         if request.POST.get("export"):
    #             response = HttpResponse(mimetype="text/csv")
    #             fname = "%s-%s.csv" % (form.slug, slugify(datetime.now().ctime()))
    #             response["Content-Disposition"] = "attachment; filename=%s" % fname
    #             queue = StringIO()
    #             csv = writer(queue, delimiter=EVALUATIONS_CSV_DELIMITER)
    #             csv.writerow(entries_form.columns())
    #             for row in entries_form.rows(csv=True):
    #                 csv.writerow(row)
    #                 # Decode and reencode the response into utf-16 to be Excel compatible
    #                 data = queue.getvalue().decode("utf-8").encode("utf-16")
    #                 response.write(data)
    #             return response
    #         elif request.POST.get("delete") and can_delete_entries:
    #             selected = request.POST.getlist("selected")
    #             if selected:
    #                 try:
    #                     from django.contrib.messages import info
    #                 except ImportError:
    #                     def info(request, message, fail_silently=True):
    #                         request.user.message_set.create(message=message)
    #                 entries = Response.objects.filter(id__in=selected)
    #                 count = entries.count()
    #                 if count > 0:
    #                     entries.delete()
    #                     message = ungettext("1 entry deleted",
    #                                         "%(count)s entries deleted", count)
    #                     info(request, message % {"count": count})
    #     template = "admin/forms/entries.html"
    #     context = {"title": _("View Entries"), "entries_form": entries_form,
    #                "opts": self.model._meta, "original": form,
    #                "can_delete_entries": can_delete_entries,
    #                "submitted": submitted}
    #     return render_to_response(template, context, RequestContext(request))

    # def file_view(self, request, field_entry_id):
    #     """
    #     Output the file for the requested field entry.
    #     """
    #     field_entry = get_object_or_404(Response, id=field_entry_id)
    #     path = join(fs.location, field_entry.value)
    #     response = HttpResponse(mimetype=guess_type(path)[0])
    #     f = open(path, "r+b")
    #     response["Content-Disposition"] = "attachment; filename=%s" % f.name
    #     response.write(f.read())
    #     f.close()
    #     return response


# class MyModelOptions(admin.ModelAdmin):
#     model = MyModel
#     # define the raw_id_fields
#     raw_id_fields = ('related_fk','related_m2m',)
#     # define the related_lookup_fields
#     related_lookup_fields = {
#         'fk': ['related_fk'],
#         'm2m': ['related_m2m'],
#     }
class MyModelOptions(admin.ModelAdmin):
    # define the raw_id_fields
    raw_id_fields = ('related_fk','related_m2m',)
    # define the autocomplete_lookup_fields
    autocomplete_lookup_fields = {
        'fk': ['related_fk'],
        'm2m': ['related_m2m'],
    }


admin.site.register(MyModel, MyModelOptions)
admin.site.register(RelatedModel)
admin.site.register(Template, TemplateAdmin)
admin.site.register(Question)
admin.site.register(Section, SectionAdmin)
admin.site.register(Option)
