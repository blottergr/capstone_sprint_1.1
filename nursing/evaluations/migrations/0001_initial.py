# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):
    
    def forwards(self, orm):
        
        # Adding model 'Template'
        db.create_table('evaluations_template', (
            ('start_date', self.gf('django.db.models.fields.DateField')()),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('end_date', self.gf('django.db.models.fields.DateField')()),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal('evaluations', ['Template'])

        # Adding model 'Section'
        db.create_table('evaluations_section', (
            ('position', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('date_created', self.gf('django.db.models.fields.DateField')(auto_now_add=True, blank=True)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('template', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['evaluations.Template'])),
            ('label', self.gf('django.db.models.fields.CharField')(max_length=300)),
        ))
        db.send_create_signal('evaluations', ['Section'])

        # Adding model 'Question'
        db.create_table('evaluations_question', (
            ('field_type', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('section', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['evaluations.Section'])),
            ('required', self.gf('django.db.models.fields.BooleanField')(default=True, blank=True)),
            ('label', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('position', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('evaluations', ['Question'])

        # Adding M2M table for field options on 'Question'
        db.create_table('evaluations_question_options', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('question', models.ForeignKey(orm['evaluations.question'], null=False)),
            ('option', models.ForeignKey(orm['evaluations.option'], null=False))
        ))
        db.create_unique('evaluations_question_options', ['question_id', 'option_id'])

        # Adding model 'Option'
        db.create_table('evaluations_option', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal('evaluations', ['Option'])

        # Adding model 'Answer'
        db.create_table('evaluations_answer', (
            ('question', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['evaluations.Question'])),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=2000)),
            ('response', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['evaluations.Response'])),
        ))
        db.send_create_signal('evaluations', ['Answer'])

        # Adding model 'Response'
        db.create_table('evaluations_response', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('template', self.gf('django.db.models.fields.related.ForeignKey')(related_name='form', to=orm['evaluations.Template'])),
            ('submit_time', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal('evaluations', ['Response'])

        # Adding model 'RelatedModel'
        db.create_table('evaluations_relatedmodel', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=140)),
        ))
        db.send_create_signal('evaluations', ['RelatedModel'])

        # Adding model 'MyModel'
        db.create_table('evaluations_mymodel', (
            ('related_fk', self.gf('django.db.models.fields.related.ForeignKey')(related_name='related_fk', to=orm['evaluations.RelatedModel'])),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=140)),
        ))
        db.send_create_signal('evaluations', ['MyModel'])

        # Adding M2M table for field related_m2m on 'MyModel'
        db.create_table('evaluations_mymodel_related_m2m', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('mymodel', models.ForeignKey(orm['evaluations.mymodel'], null=False)),
            ('relatedmodel', models.ForeignKey(orm['evaluations.relatedmodel'], null=False))
        ))
        db.create_unique('evaluations_mymodel_related_m2m', ['mymodel_id', 'relatedmodel_id'])
    
    
    def backwards(self, orm):
        
        # Deleting model 'Template'
        db.delete_table('evaluations_template')

        # Deleting model 'Section'
        db.delete_table('evaluations_section')

        # Deleting model 'Question'
        db.delete_table('evaluations_question')

        # Removing M2M table for field options on 'Question'
        db.delete_table('evaluations_question_options')

        # Deleting model 'Option'
        db.delete_table('evaluations_option')

        # Deleting model 'Answer'
        db.delete_table('evaluations_answer')

        # Deleting model 'Response'
        db.delete_table('evaluations_response')

        # Deleting model 'RelatedModel'
        db.delete_table('evaluations_relatedmodel')

        # Deleting model 'MyModel'
        db.delete_table('evaluations_mymodel')

        # Removing M2M table for field related_m2m on 'MyModel'
        db.delete_table('evaluations_mymodel_related_m2m')
    
    
    models = {
        'evaluations.answer': {
            'Meta': {'object_name': 'Answer'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['evaluations.Question']"}),
            'response': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['evaluations.Response']"}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '2000'})
        },
        'evaluations.mymodel': {
            'Meta': {'object_name': 'MyModel'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '140'}),
            'related_fk': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'related_fk'", 'to': "orm['evaluations.RelatedModel']"}),
            'related_m2m': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'related_m2m'", 'symmetrical': 'False', 'to': "orm['evaluations.RelatedModel']"})
        },
        'evaluations.option': {
            'Meta': {'object_name': 'Option'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        },
        'evaluations.question': {
            'Meta': {'object_name': 'Question'},
            'field_type': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'options': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'options'", 'blank': 'True', 'to': "orm['evaluations.Option']"}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'required': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'blank': 'True'}),
            'section': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['evaluations.Section']"})
        },
        'evaluations.relatedmodel': {
            'Meta': {'object_name': 'RelatedModel'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '140'})
        },
        'evaluations.response': {
            'Meta': {'object_name': 'Response'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'submit_time': ('django.db.models.fields.DateTimeField', [], {}),
            'template': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'form'", 'to': "orm['evaluations.Template']"})
        },
        'evaluations.section': {
            'Meta': {'object_name': 'Section'},
            'date_created': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'label': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'position': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'template': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['evaluations.Template']"})
        },
        'evaluations.template': {
            'Meta': {'object_name': 'Template'},
            'end_date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'start_date': ('django.db.models.fields.DateField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '1'})
        }
    }
    
    complete_apps = ['evaluations']
